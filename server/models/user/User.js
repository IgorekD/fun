import mongoose from 'mongoose';
import validator from './Validator';

//User Schema
const UserSchema = mongoose.Schema({
    username: {
        type: String,
        lowercase: true,
        required: [true, 'Username is required field'],
        index: {
            unique: true
        },
        validate: validator.username
    },
    firstname: {
        type: String,
        required: [true, 'Firstname is required field'],
        validate: validator.firstname
    },
    lastname: {
        type: String,
        validate: validator.lastname
    },
    password: {
        type: String,
        required: [true, 'Password is required field'],
        validate: validator.password
    },
    email: {
        type: String,
        lowercase: true,
        required: [true, 'Email is required field'],
        index: {
            unique: true
        },
        validate: validator.email
    },
    gender: {
        type: String,
        lowercase: true,
        enum: ['male', 'female'],
        required: [true, 'Gender is required field']
    },
    birth: {
        type: String,
        validate: validator.birth
    },
    father: {
        type: String
    },
    mother: {
        type: String
    }
});

const User = mongoose.model('User', UserSchema, 'Users');

User.saveUser = (user) => {
    return new Promise((resolve, reject) => {
        user.save((err, data) => {
            //check if it's duplicate error
            if (err && err.code === 11000) {
                reject('User already exist.');
            }
            if (err && err.errors) {
                const errorsKeys = Object.keys(err.errors);
                const errorsValues = Object.values(err.errors);

                const errorObject = errorsKeys.map((errorKey, i) => {
                    return {
                        field: errorKey,
                        message: errorsValues[i].message
                    }
                });
                reject(errorObject);
            } else {
                resolve(data);
            }
        });
    });
};

export default User;