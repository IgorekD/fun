import validate from 'mongoose-validator';
import moment from 'moment';

const username = [
    validate({
        validator: 'isAlphanumeric',
        passIfEmpty: false,
        message: 'Username should contain alpha-numeric characters only'
      }),
      validate({
        validator: 'isLength',
        arguments: [0, 16],
        message: 'Username should be less than {ARGS[1]} characters'
    })
];

const firstname = [
    validate({
        validator: 'matches',
        passIfEmpty: false,
        arguments: [/[a-zA-Z]+/, 'i'],
        message: 'Firstname should contain characters only and can\'t be empty'
    }),
    validate({
        validator: 'isLength',
        arguments: [0, 16],
        message: 'Firstname should be less than {ARGS[1]} characters'
    })
];

const lastname = [
    validate({
        validator: 'matches',
        passIfEmpty: true,
        arguments: [/[a-zA-Z]+/, 'i'],
        message: 'Lastname should contain characters only'
    }),
    validate({
        validator: 'isLength',
        arguments: [0, 16],
        message: 'Lastname should be less than {ARGS[1]} characters'
    })
];

const password = [
    validate({
        validator: 'isLength',
        arguments: [6, 16],
        message: 'Password should be between {ARGS[0]} and {ARGS[1]} characters'
    }),
    validate({
        validator: 'matches',
        passIfEmpty: false,
        arguments: [/^[A-Za-z\d$@$!%*#?&]{6,}$/, 'i'],
        message: 'Password should contain characters only'
    })
];

const email = [
    validate({
        validator: 'matches',
        passIfEmpty: false,
        arguments: [/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'i'],
        message: 'Email is not correct'
    })
]

const birth = [
    validate({
        validator: (val) => {
            return moment(val, 'DD-MM-YYYY').isValid();
        },
        passIfEmpty: false,
        message: 'Birth is not correct'
    })
]

const validator = { username, firstname, lastname, password, email, birth };

export default validator;