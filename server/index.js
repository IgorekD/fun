import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import User from './models/user/User';

const uri = 'mongodb://IgorekD:Password123@cluster0-shard-00-00-z7xju.mongodb.net:27017,cluster0-shard-00-01-z7xju.mongodb.net:27017,cluster0-shard-00-02-z7xju.mongodb.net:27017/app?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin';
const app = express();

//start server
app.listen(3000);

mongoose.connect(uri, {useMongoClient: true});
const db = mongoose.connection;

db.once('open', () => {
    console.log('connected to MongoDB');
});

db.once('error', (error) => {
    console.log(error);
});

app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.get('/', function(req, res) {
    
    User.find({}, (error, users) => {
        if (error) throw new Error(error);
        return res.send(JSON.stringify(users));
    });
});

app.post('/login', (req, res) => {
    User.findOne({username: req.body.username, password: req.body.password}, (err, user) => {
        if (err) throw (err);

        if (user) {
            res.send(user);
        } else {
            res.status(400).send({error: 'Current password does not match'});
        }
    });
});

app.post('/register', (request, response) => {

    const user = new User(request.body);
    
    User.saveUser(user).then(data => {
        response.send({message: 'User ' + data.username + ' successfully added!!!'});
    }, error => {
        response.status(500).send({data: error, error: true});
    });
});