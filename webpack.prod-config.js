const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CompressionPlugin  = require('compression-webpack-plugin');

const config = {
    context: path.resolve(__dirname + '/src'),
    entry: {
        app: './index.jsx'
    },
    output: {
        path: path.resolve(__dirname + '/dist/assets'),
        filename: '[name].js',
        publicPath: '/tourViewer/assets/',
        chunkFilename : '[name]-[id].js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: '../index.html',
            template: 'index.html'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: function(module) {
                var context = module.context;

                if (typeof context !== 'string') {
                    return false;
                }

                return context.indexOf('node_modules') !== -1;
            }
        }),
        new UglifyJsPlugin({
            beautify: false,
            comments: false,
            sourceMap: false,
            compress: {
                sequences: true,
                booleans: true,
                loops: true,
                unused: true,
                warnings: false,
                drop_console: false,// set to true in production verison!!!
                unsafe: true
            }
        }),
        new CompressionPlugin({
            asset: '[path].gz[query]',
            algorithm: 'gzip',
            test: /\.(js|html)$/,
            minRatio: 0.9
        }),
        new webpack.ProvidePlugin({
            'Promise': 'es6-promise',
            'fetch': 'imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch'
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '\"production\"'
        })
    ],
    devtool: 'source-map',// remove in production verison!!!
    module: {
        rules: [
            {
                test: /\.(jsx|js)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                    }
                ]
            },
            {
                test: /\.css$/,
                // exclude: /node_modules/,
                use: [ 'style-loader', 'css-loader', 'postcss-loader' ]
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader']
            },
            {
                test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                exclude: /node_modules\/(?!(slick-carousel)\/).*/,
                use: [
                    {
                        loader: 'file-loader',
                        query: {
                            name:'fonts/[name].[ext]'
                        }
                    }
                 ]
            },
            {
                test: /\.(gif|png|jpe?g)$/i,
                exclude: /node_modules\/(?!(slick-carousel)\/).*/,
                loaders: [
                    {
                        loader: 'file-loader',
                        query: {
                            name:'images/[name].[ext]'
                        }
                    },
                    {
                        loader: 'image-webpack-loader',
                        query: {
                            mozjpeg: {
                                progressive: true
                            },
                            gifsicle: {
                                interlaced: false
                            },
                            optipng: {
                                optimizationLevel: 7
                            },
                            pngquant: {
                                quality: '65-90',
                                speed: 4
                            }
                        }
                    }
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx', '.css', '.scss', '.png', '.jpg', '.jpeg', '.gif']
    }
};

module.exports = config;