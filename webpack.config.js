const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const config = {
    context: path.resolve(__dirname, 'src'),
    entry: [
        'react-hot-loader/patch',
        './index.jsx'
    ],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js',
        chunkFilename : '[name]-[id].js',
        publicPath: 'http://localhost:8080/'
    },
    devtool: 'source-map',
    devServer: {
        contentBase: path.join(__dirname, 'src'),
        port: 8080,
        historyApiFallback: true,
        hot: true,
        overlay: true,
        publicPath: 'http://localhost:8080/'
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.html'
        }),
        // new BundleAnalyzerPlugin({
        //     analyzerMode: 'static'
        // }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: function(module) {
                var context = module.context;

                if (typeof context !== 'string') {
                    return false;
                }

                return context.indexOf('node_modules') !== -1;
            }
        }),
        new webpack.ProvidePlugin({
            'Promise': 'es6-promise',
            'fetch': 'imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch'
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '\"development\"'
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin()
    ],
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.js?x$/,
                exclude: /node_modules/,
                loader: 'eslint-loader'
            },
            {
                test: /\.(jsx|js)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                    }
                ]
            },
            {
                test: /\.css$/,
                exclude: /node_modules\/(?!(slick-carousel)\/).*/,
                use: [ 'style-loader', 'css-loader', 'postcss-loader' ]
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    'style-loader',
                    'css-loader',
                    'postcss-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                exclude: /node_modules\/(?!(slick-carousel)\/).*/,
                use: [
                    {
                        loader: 'file-loader',
                        query: {
                            name:'fonts/[name].[ext]'
                        }
                    }
                 ]
            },
            {
                test: /\.(gif|png|jpe?g)$/i,
                exclude: /node_modules\/(?!(slick-carousel)\/).*/,
                loaders: [
                    {
                        loader: 'file-loader',
                        query: {
                            name:'images/[name].[ext]'
                        }
                    }
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx', '.css', '.scss', '.png', '.jpg', '.jpeg', '.gif']
    }
};

module.exports = config;