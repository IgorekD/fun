import * as consts from './UserConsts';

export const toggleForm = () => {
    return (dispatch, getState) => {
        dispatch({
            type: consts.TOGGLE_FORM,
            payload: !getState().user.disabled
        });
    }
};

export const toggleModal = () => {
    return (dispatch, getState) => {
        dispatch({
            type: consts.TOGGLE_MODAL,
            payload: !getState().user.isModalOpen
        })
    }
};

export const changeInfo = (name, value) => {
    return (dispatch, getState) => {
        dispatch({
           type: consts.CHANGE_INFO,
           payload: {
               name,
               value
           }
        })
    }
};