import * as consts from './UserConsts';

const initialState = {
    disabled: true,
    isModalOpen: false,
    userInfo: {
        firstName: 'Adolf',
        lastName: 'Hitler',
        email: 'fuhrer@nazi.com',
        gender: 'Dictator',
        birth: '20 April 1889',
        father: 'Alois Hitler Sr',
        mother: 'Maria Anna Schicklgruber'
    }
};

const user = (state = initialState, action) => {
    switch (action.type) {
        case consts.TOGGLE_FORM:
            return {
                ...state,
                disabled: action.payload
            };
        case consts.TOGGLE_MODAL:
            return {
                ...state,
                isModalOpen: action.payload
            };
        case consts.CHANGE_INFO:
            return {
                ...state,
                userInfo: {
                    ...state.userInfo,
                    [action.payload.name]: action.payload.value
                }
            };
        default:
            return state;
    }
};

export default user;