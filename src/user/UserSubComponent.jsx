import React, {Component} from 'react';

import {Link} from 'react-router-dom'

import tiger from '../assets/tiger.jpg'

class UserSubComponent extends Component {
    render() {
        return (
            <div className="sub-container">
                <div className="sub-container__img">
                    <img src={tiger} alt=""/>
                </div>
                <div className="back">
                    <Link to="/user/">
                        {'<<< BACK'}
                    </Link>
                </div>
            </div>
        )
    }
}

export default UserSubComponent;