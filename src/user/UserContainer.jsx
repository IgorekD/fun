import React, {Component} from 'react';
import ReactDOM from 'react-dom'

import {Switch, Route} from 'react-router-dom'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import UserInfoComponent from './UserInfoComponent';
import UserActionsComponent from './UserActionsComponent';
import UserSubComponent from './UserSubComponent';
import ModalComponent from '../helpers/ModalComponent';

import * as userActions from './UserActions'

import './Style';


class UserContainer extends Component {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.handleChange = this.handleChange.bind(this)
    }

    handleClick() {
       this.props.userActions.toggleForm();
    }

    toggleModal() {
        this.props.userActions.toggleModal();
    }

    handleChange(e) {
        // this.setState({[e.target.name]: e.target.value});
        this.props.userActions.changeInfo(e.target.name, e.target.value);
    }

    render() {
        return (
            <div className="wrapper">
                <UserInfoComponent
                    inputsEnable={this.props.disabled}
                    userInfo={this.props.userInfo}
                    handleChange={this.handleChange}
                />
                <Switch>
                    <Route
                        exact
                        path="/user/"
                        render={
                                routePops => (
                                    <UserActionsComponent
                                        {...routePops}
                                        disabled={this.handleClick}
                                        toggleModal={this.toggleModal}/>
                                )
                            }
                    />
                    <Route path="/user/sub/" component={UserSubComponent}/>
                </Switch>
                <div>
                    {this.props.isModalOpen &&
                        ReactDOM.createPortal(
                            <ModalComponent toggleModal={this.toggleModal}>
                                <h1>Sieg Heil!!!</h1>
                            </ModalComponent>,
                            document.getElementById('portal')
                        )
                    }
                </div>
            </div>

        )
    }

}

const mapStateToProps = (state) => {
    return {
        disabled: state.user.disabled,
        isModalOpen: state.user.isModalOpen,
        userInfo: state.user.userInfo
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userActions: bindActionCreators(userActions, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(UserContainer)

