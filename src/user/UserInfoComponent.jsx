import React, { Component } from 'react';
import dictator from '../assets/634.jpg';

class UserInfoComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            counter: 0,
            myFriends: ['Igor', 'Igor2', 'Igor', 'Igor', 'Igor', 'Dima', 'Dima' ,'Dima' ,'Dima']
        };

        this.counterClick = this.counterClick.bind(this);
    }


    counterClick(e) {
        this.setState({
            counter: this.state.counter + +e.target.value
        })
    }

    render () {

        const userForm = (title, value, name) => {
            return (
                <div className="user-name__input">
                    <label>
                        {title}
                    </label>
                    <input
                        type="text"
                        onChange={this.props.handleChange}
                        value={value}
                        name={name}
                        disabled={this.props.inputsEnable ? 'disabled' : ''}/>
                </div>
            );
        };

        const friendsBlock = this.state.myFriends.map(
            (myFriends, i) =>(
                <li key={i}>
                    {myFriends}
                </li>
            )
        );

        return (
            <div className="user">
                <div className="user-photo">
                    <img src={dictator} alt=""/>
                </div>
                <div className="friends">
                    {friendsBlock}
                </div>
                <div className="user-name">
                    {userForm('First Name', this.props.userInfo.firstName, 'firstName')}
                    {userForm('Last Name', this.props.userInfo.lastName, 'lastName')}
                    {userForm('Email', this.props.userInfo.email, 'email')}
                    {userForm('Gender', this.props.userInfo.gender, 'gender')}
                    {userForm('Birth', this.props.userInfo.birth, 'birth')}
                    {userForm('Father', this.props.userInfo.father, 'father')}
                    {userForm('Mother', this.props.userInfo.mother, 'mother')}
                </div>
                <div className="counter">
                    <div className="counter-wrapper">
                        <span>{this.state.counter}</span>
                        <div className="counter__btn">
                            <button
                                onClick={this.counterClick}
                                value='-1'
                                disabled={(this.state.counter === 0) ? true : ''}>
                                -
                            </button>
                            <button
                                onClick={this.counterClick}
                                value='1'
                                disabled={(this.state.counter === 100) ? true : ''}>
                                +
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default UserInfoComponent;