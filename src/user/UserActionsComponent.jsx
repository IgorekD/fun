import React, {Component} from 'react';

import {Link} from 'react-router-dom';

class UserActionsComponent extends Component {

    constructor() {
        super();

        this.state = {
            password: '',
            passwordRepeat: '',
            validatePass: true
        };

        this.onChangePassword = this.onChangePassword.bind(this);
        this.validatePassword = this.validatePassword.bind(this);
    }

    onChangePassword(e) {
        this.setState({
            [e.target.name]: e.target.value,
            validatePass: true
        })
    }

    validatePassword() {
        if (this.state.password === this.state.passwordRepeat) {
            this.setState({validatePass: true});
        } else {
            this.setState({validatePass: false});
        }

    }

    render() {
        return (
            <div className="user-actions">
                <div className={this.state.validatePass ? 'password' : 'password password--error'}>
                    <h4>Change password</h4>
                    <input
                        type="password"
                        value={this.state.password}
                        name='password'
                        onChange={this.onChangePassword}
                    />
                    <input
                        type="password"
                        value={this.state.passwordRepeat}
                        name='passwordRepeat'
                        onChange={this.onChangePassword}
                    />
                    <button onClick={this.validatePassword}>
                        Change
                    </button>
                </div>
                <div className="button-block">
                    <button onClick={this.props.toggleModal}>
                        Change photo
                    </button>
                    <button onClick={this.props.disabled}>
                        Change information
                    </button>
                    <Link to='/user/sub/'>
                        <button>
                            Open photo
                        </button>
                    </Link>
                </div>
            </div>
        )
    }
}

export default UserActionsComponent;