
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import InboxIcon from 'material-ui-icons/MoveToInbox';
import DraftsIcon from 'material-ui-icons/Drafts';
import StarIcon from 'material-ui-icons/Star';
import SendIcon from 'material-ui-icons/Send';
import MailIcon from 'material-ui-icons/Mail';
import DeleteIcon from 'material-ui-icons/Delete';
import ReportIcon from 'material-ui-icons/Report';
import Divider from 'material-ui/Divider';

const styles = {
    list: {
      width: 250
    },
    listFull: {
      width: 'auto'
    }
  };

class MenuContainer extends Component {
    constructor() {
        super();


        this.state = {
            drawerIsOpen: false
        }
    }

    render() {

        const { classes, drawerIsOpen, toggleDrawer } = this.props;

        return (
            <Drawer open={drawerIsOpen} onClose={() => {toggleDrawer(false)}}>
                <div
                    tabIndex={0}
                    role="button"
                    onClick={() => toggleDrawer(false)}
                    onKeyDown={() => toggleDrawer(false)}
                >
                    <div className={classes.list}>
                        <List>
                            <div>
                                <ListItem button>
                                <ListItemIcon>
                                    <InboxIcon />
                                </ListItemIcon>
                                <ListItemText primary="Inbox" />
                                </ListItem>
                                <ListItem button>
                                <ListItemIcon>
                                    <StarIcon />
                                </ListItemIcon>
                                <ListItemText primary="Starred" />
                                </ListItem>
                                <ListItem button>
                                <ListItemIcon>
                                    <SendIcon />
                                </ListItemIcon>
                                <ListItemText primary="Send mail" />
                                </ListItem>
                                <ListItem button>
                                <ListItemIcon>
                                    <DraftsIcon />
                                </ListItemIcon>
                                <ListItemText primary="Drafts" />
                                </ListItem>
                            </div>
                        </List>
                        <Divider />
                        <List>
                            <div>
                                <ListItem button>
                                <ListItemIcon>
                                    <MailIcon />
                                </ListItemIcon>
                                <ListItemText primary="All mail" />
                                </ListItem>
                                <ListItem button>
                                <ListItemIcon>
                                    <DeleteIcon />
                                </ListItemIcon>
                                <ListItemText primary="Trash" />
                                </ListItem>
                                <ListItem button>
                                <ListItemIcon>
                                    <ReportIcon />
                                </ListItemIcon>
                                <ListItemText primary="Spam" />
                                </ListItem>
                            </div>
                        </List>
                    </div>
                </div>
            </Drawer>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(MenuContainer));