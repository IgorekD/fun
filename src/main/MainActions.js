import { getUserReq, loginReq } from '../helpers/requestUtils';
import * as consts from './MainConsts';

export const getUser = () => {
    return dispatch => {
        dispatch({type: consts.GET_USER_PENDING});

        return getUserReq().then((data) => {
            dispatch({
                type: consts.GET_USER_FULFILLED,
                payload: data
            });
        }, (error) => {
            dispatch({
                type: consts.GET_USER_REJECTED,
                payload: error
            });
        });
    }
}

export const login = (username, password) => {
    return dispatch => {
        dispatch({type: consts.LOGIN_PENDING});

        return loginReq(username, password).then((data) => {
            dispatch({
                type: consts.LOGIN_FULFILLED,
                payload: data.username
            });
        }, (error) => {
            dispatch({
                type: consts.LOGIN_REJECTED,
                payload: error.error
            });
        });
    }
}