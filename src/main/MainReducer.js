import * as consts from './MainConsts';

const initialState = {
    loginError: '',
    loginIsPending: false,
    username: ''
};

const main = (state = initialState, action) => {
    switch (action.type) {
        case consts.LOGIN_PENDING:
            return {
                ...state,
                loginIsPending: true
            }
        case consts.LOGIN_REJECTED:
            return {
                ...state,
                loginError: action.payload,
                loginIsPending: false
            }
        case consts.LOGIN_FULFILLED:
            return {
                ...state,
                username: action.payload,
                loginError: '',
                loginIsPending: false
            }
        default:
            break;
    }
    return state;
}

export default main;
