import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './Style';
import { Link } from 'react-router-dom';
import * as MainActions from './MainActions';

class MainContainer extends Component {
    constructor() {
        super();

        this.state = {
            username: '',
            password: ''
        }

        this.getUserClick = this.getUserClick.bind(this);
        this.inputOnChange = this.inputOnChange.bind(this);
    }

    inputOnChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    getUserClick() {
        this.props.mainActions.login(this.state.username, this.state.password);
    }
    
    render() {

        const { username, loginError } = this.props;

        const usernameBlock = username.length
            ? (
                <h2>{username}</h2>
            )
            : null;

        const loginErrorBlock = loginError.length
            ? (
                <h2>{loginError}</h2>
            )
            : null;

        return (
            <div
                className="main-container">
                <h1 className="main-title">Main Page</h1>
                {usernameBlock}
                {loginErrorBlock}
                <div className={loginError.length ? 'login-form has-error' : 'login-form'}>
                    <div className="form-group">
                        <label htmlFor='username'>Username</label>
                        <input
                            type='text'
                            id='username'
                            name='username'
                            onChange={this.inputOnChange}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor='password'>Password</label>
                        <input
                            type='text'
                            id='password'
                            name='password'
                            onChange={this.inputOnChange}/>
                    </div>
                    <div className="form-group">
                        <button className="btn" onClick={this.getUserClick}>Login</button>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loginError: state.main.loginError,
        username: state.main.username
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        mainActions: bindActionCreators(MainActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer)