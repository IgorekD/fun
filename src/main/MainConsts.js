export const GET_USER_PENDING = 'getUserPendingMain';
export const GET_USER_REJECTED = 'getUserRejectedMain';
export const GET_USER_FULFILLED = 'getUserFulfilledMain';

export const LOGIN_PENDING = 'loginPendingMain';
export const LOGIN_REJECTED = 'loginRejectedMain';
export const LOGIN_FULFILLED = 'loginFulfilledMain';