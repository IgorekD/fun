import React, { Component } from 'react'

export default class ModalComponent extends Component {
    render() {
       return (
               <div className="modal">
                   <button
                       className="modal__close"
                       onClick={this.props.toggleModal}
                   >
                       Close
                   </button>
                   {this.props.children}
               </div>
       );
    }
}
