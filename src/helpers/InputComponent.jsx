import React, {Component} from 'react'

export default class InputComponent extends Component {
    render() {
        const { inputName, inputTitle, inputType, inputChange} = this.props;
        return (
            <div className="form-group">
                <label htmlFor={inputName}>{inputTitle}</label>
                <input
                    type={inputType}
                    id={inputName}
                    name={inputName}
                    onChange={inputChange}
                />
            </div>
        );
    }
}
