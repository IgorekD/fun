export const getUserReq = () => {
    return new Promise((resolve, reject) => {
        fetch('http://localhost:3000/').then(response => {
            if (response.status === 401 || response.status === 400) {
                response.json().then(res => {
                    reject(res);
                });
            } else {
                response.json().then(
                    result => {
                        resolve(result);
                    },
                    err => {
                        reject(err);
                    }
                );
            }
        })
            .catch(reject);
    });
};

export const loginReq = (username, password) => {
    return new Promise((resolve, reject) => {
        fetch('http://localhost:3000/login',
            {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    username,
                    password
                })
            }
        ).then(response => {
            if (response.status === 401 || response.status === 400 || response.status === 500) {
                response.json().then(res => {
                    reject(res);
                });
            } else {
                response.json().then(
                    result => {
                        resolve(result);
                    },
                    err => {
                        reject(err);
                    }
                );
            }
        })
            .catch(reject);
    });
};

export const registerUserReq = (data) => {
    return new Promise((resolve, reject) => {
        fetch('http://localhost:3000/register',
            {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }).then(response => {
            if (response.status === 401 || response.status === 400 || response.status === 500) {
                response.json().then(res => {
                    reject(res);
                });
            } else {
                response.json().then(
                    result => {
                        resolve(result);
                    },
                    err => {
                        reject(err);
                    }
                );
            }
        })
            .catch(reject);
    })
};