import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import { Route } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import createLogger from 'redux-logger';

import rootReducer from './rootReducer';
import { AppContainer  as ReactContainer } from 'react-hot-loader';
import AppContainer from './app/AppContainer';

let basename = '';

if (process.env.NODE_ENV === 'production') {
    basename = '/tourViewer';
}

const history = createBrowserHistory({
    basename: basename
});

const routersMid = routerMiddleware(history);
const logger = createLogger();

const configureStore = () => {
    const store = createStore(rootReducer, applyMiddleware(routersMid, thunk, promiseMiddleware(), logger));

    if (module.hot) {
    module.hot.accept('./rootReducer', () => {
        const nextRootReducer = require('./rootReducer');
        store.replaceReducer(nextRootReducer);
    })
  }

  return store;
}

const store = configureStore();

const render = component => {
    const contentRoot = document.getElementById('app')
    ReactDOM.render(
        <ReactContainer>
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Route component={component}/>
                </ConnectedRouter>
            </Provider>
        </ReactContainer>
    , contentRoot);
}

render(AppContainer);

if (module.hot) {
    module.hot.accept('./app/AppContainer', () => {
        const NextRoutes = require('./app/AppContainer').default;
        render(NextRoutes);
    });
}
