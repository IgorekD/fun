export const REGISTER_USER_PENDING = 'registerUserRegistrationPending';
export const REGISTER_USER_REJECTED = 'registerUserRegistrationRejected';
export const REGISTER_USER_FULFILLED = 'registerUserRegistrationFulfilled';
export const CHANGE_INPUT = 'registerUserChangeInput';