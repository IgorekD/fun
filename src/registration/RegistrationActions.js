import React from 'react'
import * as consts from './RegistrationConsts';
import { registerUserReq } from '../helpers/requestUtils';
import {NotificationManager} from 'react-notifications';

export const registrationBtnClick = () => {
    return (dispatch, getState) => {
        dispatch({type: consts.REGISTER_USER_PENDING});
        const username = getState().registration.username;
        const password = getState().registration.password;
        const firstname = getState().registration.firstname;
        const lastname = getState().registration.lastname;
        const email = getState().registration.email;
        const gender = getState().registration.gender;
        const birth = getState().registration.birth;
        const father = getState().registration.father;
        const mother = getState().registration.mother;
        // const { username, password } = getState.registration;

        registerUserReq({username, password, firstname, lastname,
            email, gender, birth, father, mother}).then(() => {
            dispatch({
                type: consts.REGISTER_USER_FULFILLED
            });
        }, error => {

            const errorKeys = Object.keys(error.data.errors);
            const errorValues = Object.values(error.data.errors);

            let errors = {};

            errorKeys.forEach((item, i) => {
                errors = {
                    ...errors,
                    [item + 'ErrorMsg']: errorValues[i].message
                };
                NotificationManager.error(<div>{errorValues[i].message}</div>, 'Error!', 5000);
            });

            dispatch({
                type: consts.REGISTER_USER_REJECTED,
                payload: errors
            });
        });
    }
};

export const changeInput = (name, value) => {
    return (dispatch, getState) => {
        dispatch({
            type: consts.CHANGE_INPUT,
            payload: {
                name,
                value
            }
        })
    }
};