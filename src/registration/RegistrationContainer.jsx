import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as RegistrationActions from './RegistrationActions';
import InputComponent from '../helpers/InputComponent';
import './Style';

class RegistrationContainer extends Component {
    constructor(props) {
        super(props);
        
        this.inputOnChange = this.inputOnChange.bind(this);
        this.registrationBtnClick = this.registrationBtnClick.bind(this);
    }

    inputOnChange(e) {
        this.props.registrationActions.changeInput(e.target.name, e.target.value)
    }

    registrationBtnClick() {
        this.props.registrationActions.registrationBtnClick();
    }

    render () {
        return (
            <div className="registration-container">
                <h1>Registration</h1>
                <div className="registration-form">
                    <InputComponent
                        inputType="text"
                        inputName="username"
                        inputTitle="User Name"
                        inputChange={this.inputOnChange}
                    />
                    <InputComponent
                        inputType="password"
                        inputName="password"
                        inputTitle="Password"
                        inputChange={this.inputOnChange}
                    />
                    <hr/>
                    <InputComponent
                        inputType="text"
                        inputName="firstname"
                        inputTitle="First Name"
                        inputChange={this.inputOnChange}
                    />
                    <InputComponent
                        inputType="text"
                        inputName="lastname"
                        inputTitle="Last Name"
                        inputChange={this.inputOnChange}
                    />
                    <InputComponent
                        inputType="email"
                        inputName="email"
                        inputTitle="Email"
                        inputChange={this.inputOnChange}
                    />
                    <InputComponent
                        inputType="text"
                        inputName="gender"
                        inputTitle="Gender"
                        inputChange={this.inputOnChange}
                    />
                    <InputComponent
                        inputType="text"
                        inputName="birth"
                        inputTitle="Birth"
                        inputChange={this.inputOnChange}
                    />
                    <InputComponent
                        inputType="text"
                        inputName="father"
                        inputTitle="Father"
                        inputChange={this.inputOnChange}
                    />
                    <InputComponent
                        inputType="text"
                        inputName="mother"
                        inputTitle="Mother"
                        inputChange={this.inputOnChange}
                    />
                    <div className="form-group">
                        <button className="btn" onClick={this.registrationBtnClick}>Registration</button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        registrationActions: bindActionCreators(RegistrationActions, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationContainer)