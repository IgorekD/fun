import * as consts from './RegistrationConsts';

const initialState = {
    registerUserPending: false,
    username: '',
    password: '',
    firstname: '',
    lastname: '',
    email: '',
    gender: '',
    birth: '',
    father: '',
    mother: '',
    errors: {
        usernameErrorMsg: '',
        emailErrorMsg: '',
        genderErrorMsg: '',
        firstnameErrorMsg: '',
        passwordErrorMsg: ''
    }
};

const registration = (state = initialState, action) => {
    switch (action.type) {
        case consts.REGISTER_USER_PENDING:
            return {
                ...state,
                registerUserPending: true
            };
        case consts.REGISTER_USER_FULFILLED:
            return {
                ...state,
                registerUserPending: false
            };
        case consts.REGISTER_USER_REJECTED:
            return {
                ...state,
                registerUserPending: false,
                errors: {
                    ...state.errors,
                    ...action.payload
                }
            };
        case consts.CHANGE_INPUT:
            return {
                ...state,
                [action.payload.name]: action.payload.value
            };
        default:
            return state
    }
};

export default registration;