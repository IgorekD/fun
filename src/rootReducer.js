import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import main from './main/MainReducer';
import app from './app/AppReducer';
import registration from './registration/RegistrationReducer';
import user from './user/UserReducer';

import * as appConsts from './app/AppConsts';

const appReducer = combineReducers({
    main,
    app,
    registration,
    user,
    routing: routerReducer
});

const rootReducer = (state, action) => {
    switch (action.type) {
        default:
            break;
    }

    if (action.type === '') {
        console.log(action.payload);
    }
    return appReducer(state, action)
}

export default rootReducer;
