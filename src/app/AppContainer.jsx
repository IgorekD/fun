import 'babel-polyfill';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import AppComponent from './AppComponent';
import * as appActions from './AppActions';
import './Style';

class AppContainer extends Component {
    constructor() {
        super();
    }

    render() {
        return <AppComponent />;
    }
}

const mapStateToProps = (state) => {
    return {
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        appActions: bindActionCreators(appActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer)
