import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom';

import MainContainer from '../main/MainContainer';
import RegistrationContainer from '../registration/RegistrationContainer';
import UserContainer from '../user/UserContainer';
import HeaderContainer from './header/HeaderContainer';

class AppComponent extends Component {

    render() {
        return (
            <div
                className="app">
                <HeaderContainer />
                <main>
                    <Switch>
                        <Route path='/' exact component={MainContainer}/>
                        <Route path='/user' component={UserContainer}/>
                        <Route path='/registration' component={RegistrationContainer}/>
                    </Switch>
                </main>
            </div>
        )
    }
}

export default AppComponent;

