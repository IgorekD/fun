export const CHANGE_FULLSCREEN = 'changeFullscreenApp';
export const SET_URL_PARAMS = 'setUrlParamsApp';
export const GET_TOUR_PENDING = 'getTourAppPending';
export const GET_TOUR_FULFILLED = 'getTourAppFulfilled';
export const GET_TOUR_REJECTED = 'getTourAppRejected';
export const SET_CONTENT_BLUR = 'setContentBlurApp';