import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import MenuContainer from '../../menu/MenuContainer';

const styles = {
    root: {
      width: '100%'
    },
    flex: {
      flex: 1
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20
    }
  };

class HeaderContainer extends Component {
    constructor() {
        super();

        this.state = {
            drawerIsOpen: false
        }

        this.toggleDrawer = this.toggleDrawer.bind(this);
    }

    toggleDrawer(value) {
        this.setState({
            drawerIsOpen: value
        });
    }

    render() {

        const { classes } = this.props;
        const { drawerIsOpen } = this.state;

        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton
                            className={classes.menuButton}
                            color="contrast"
                            aria-label="Menu"
                            onClick={() => this.toggleDrawer(true)}>
                            <MenuIcon />
                        </IconButton>
                        <Typography className={classes.flex} type="title" color="inherit">
                            Title
                        </Typography>
                        <Button color="contrast">Login</Button>
                    </Toolbar>
                </AppBar>
                <MenuContainer toggleDrawer={this.toggleDrawer} drawerIsOpen={drawerIsOpen} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(HeaderContainer));
